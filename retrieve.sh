#!/bin/bash -e

# Starting from the human, this script retrieves all orthologs and the
# distance of these genes to the telomeres. The results are stored in
# a folder with one file per species. Subsequently, all these files can be
# joined via the name of the human gene.
#
# This script accesses the Ensembl Mart database via MySQL, i.e. not
# via its Perl API and neither via the web interface. This technically
# somewhat exceptional route was initially chosen to allow it to evolve
# from regular MySQL usage for a non-programmer. Admittedly, it has then
# turned into a vivid example on why an API should be used, instead.
#
# Copyright: 2019-20 Steffen Moeller <steffen.moeller@uni-rostock.de>
# License: Public Domain
#
# If reusing this code for scientific purposes then please
# cite the publication that pointed to this repository.

# Specify version of Ensembl to use
ensemblversion=104

# Choose a directory into which the script should write its findings
outputdir=out8_retrieve_mart$ensemblversion

if ! which mysql; then
	echo "E: Expecting the mysql client to be installed. Install the 'default-mysql-client' package or otherwise make sure the mysql binary is ."
	exit -1
fi

# Nothing should require to be changed below this line

cmd="mysql -A -h martdb.ensembl.org -u anonymous -P 5316 ensembl_mart_$ensemblversion"
echo "show tables like 'hsapiens_gene_ensembl__homolog_%'" | $cmd | cut -f6 -d_ | tee species.txt

mkdir -p $outputdir

tail -n +2 species.txt | while read species
do
	echo $species
	if [ -z "$species" ]; then
		echo "E: species not defined - check!"
		break;
	fi

	table="hsapiens_gene_ensembl__homolog_${species}__dm"
	echo $table

	queryDesc="desc $table"
	#echo $queryDesc|$cmd|cut -f1
	highConfidenceAttrib=$(echo $queryDesc |$cmd | grep is_high_confidence_|cut -f1)
	descriptionAttrib=$(echo $queryDesc|$cmd|grep description_|cut -f1)
	chrNameAttrib=$(echo $queryDesc|$cmd|grep chr_name_|cut -f1)
	chrStartAttrib=$(echo $queryDesc|$cmd|grep chr_start_|cut -f1)
	chrEndAttrib=$(echo $queryDesc|$cmd|grep chr_end_|cut -f1)
	echo "highConfidenceAttrib=$highConfidenceAttrib"
	echo "descriptionAttrib=$descriptionAttrib"
	echo "chrNameAttrib=$chrNameAttrib"
	echo "chrStartAttrib=$chrStartAttrib"
	echo "chrEndAttrib=$chrEndAttrib"

	if [ -z "$chrEndAttrib" -o -z "$chrNameAttrib" -o -z "$descriptionAttrib" ]; then
		echo "E: Some attributes are not defined for species '$species' - check query!"
		break;
	fi

	query="SELECT t.gene_id_1020_key geneid,MIN(IF(i.l-$chrStartAttrib<$chrEndAttrib,i.l-$chrStartAttrib,$chrEndAttrib)) as dist,h.display_label_1074 as name, t.$chrNameAttrib as chromosome
FROM $table AS t,
     (select $chrNameAttrib, MAX($chrEndAttrib) AS l FROM $table GROUP BY $chrNameAttrib) AS i,
     hsapiens_gene_ensembl__gene__main as h
WHERE t.$chrNameAttrib=i.$chrNameAttrib AND $descriptionAttrib IN ('ortholog_one2one','ortholog_one2many','ortholog_many2many') AND h.gene_id_1020_key=t.gene_id_1020_key
GROUP BY geneid
ORDER BY geneid" 

#WHERE t.$chrNameAttrib=i.$chrNameAttrib AND $descriptionAttrib IN ('ortholog_one2one','ortholog_one2many','ortholog_many2many') AND $highConfidenceAttrib=1 AND h.gene_id_1020_key=t.gene_id_1020_key

	echo $query | $cmd > $outputdir/${species}_raw.txt
	echo
done 

query="
SELECT h.gene_id_1020_key geneid,
       MIN(IF(i.l-seq_region_start_1020<seq_region_end_1020,i.l-seq_region_start_1020,seq_region_end_1020)) as dist,
       h.display_label_1074 as name,
       h.name_1059 as chromosome
FROM hsapiens_gene_ensembl__gene__main as h,
     (select name_1059, MAX(seq_region_end_1020) AS l FROM hsapiens_gene_ensembl__gene__main GROUP BY name_1059) AS i
WHERE h.name_1059=i.name_1059
GROUP BY geneid
ORDER BY geneid
" 
	echo $query | $cmd > $outputdir/hsapiens_raw.txt
